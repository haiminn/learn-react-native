import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addTodo,
  getAllTodo,
  removeTodo,
  editTodo,
} from '../store/reducers/todosSlice';

export default function useTodos() {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todosSlice.todos);

  const lenghtOfTodos = todos.length;

  const onAddTodo = () => {
    dispatch(addTodo({ name: 'todo' + lenghtOfTodos, done: false }));
  };

  const onRemoveTodo = (id) => {
    dispatch(removeTodo({ todoId: id }));
  };

  const onChangeStatus = (todo) => {
    const todoS = {
      ...todo,
      todoId: todo._id,
      done: !todo.done,
    };
    dispatch(editTodo(todoS));
  };

  useEffect(() => {
    dispatch(getAllTodo());
  }, []);

  return {
    todos,
    onAddTodo,
    onRemoveTodo,
    onChangeStatus,
  };
}
