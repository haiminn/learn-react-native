import { createSlice } from '@reduxjs/toolkit';

export const todosSlice = createSlice({
  name: 'TODOS',
  initialState: {
    todos: [],
    messageTodo: '',
  },
  reducers: {
    getAllTodo: (state, action) => {
      return { ...state };
    },
    getAllTodoSuccess: (state, action) => {
      return { ...state, todos: action.payload };
    },
    getAllTodoFail: (state, action) => {
      return { ...state, todos: [], messageTodo: action.payload };
    },

    addTodo: (state, action) => {
      return { ...state };
    },
    addTodoSuccess: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },
    addTodoFail: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },

    removeTodo: (state, action) => {
      return { ...state };
    },
    removeTodoSuccess: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },
    removeTodoFail: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },

    editTodo: (state, action) => {
      return { ...state };
    },
    editTodoSuccess: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },
    editTodoFail: (state, action) => {
      return { ...state, messageTodo: action.payload };
    },
  },
});

export const {
  getAllTodo,
  getAllTodoSuccess,
  getAllTodoFail,

  addTodo,
  addTodoSuccess,
  addTodoFail,

  removeTodo,
  removeTodoSuccess,
  removeTodoFail,

  editTodo,
  editTodoSuccess,
  editTodoFail,
} = todosSlice.actions;

export default todosSlice.reducer;
