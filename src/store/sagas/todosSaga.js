import { call, put, takeEvery } from 'redux-saga/effects';
import getClient from '../../apiClient/client';
import {
  getAllTodo,
  getAllTodoSuccess,
  getAllTodoFail,
  addTodo,
  addTodoSuccess,
  addTodoFail,
  removeTodo,
  removeTodoSuccess,
  removeTodoFail,
  editTodo,
  editTodoSuccess,
  editTodoFail,
} from '../reducers/todosSlice';

export function* getTodo() {
  try {
    const client = getClient();
    const result = yield call(client.todo.getTodo);

    if (result.response) {
      yield put(getAllTodoSuccess(result.response));
    } else {
      yield put(getAllTodoFail(result.message));
    }
  } catch (e) {
    yield put(getAllTodoFail(e.message));
  }
}

export function* newTodo(action) {
  try {
    const client = getClient();
    const result = yield call(client.todo.addTodo, action.payload);

    if (result.response) {
      yield put(addTodoSuccess(result.message));
      yield put(getAllTodo());
    } else {
      yield put(addTodoFail(result.message));
    }
  } catch (e) {
    yield put(addTodoFail(e.message));
  }
}

export function* deleteTodo(action) {
  try {
    const client = getClient();
    const result = yield call(client.todo.removeTodo, action.payload);

    if (result.response) {
      yield put(removeTodoSuccess(result.message));
      yield put(getAllTodo());
    } else {
      yield put(removeTodoFail(result.message));
    }
  } catch (e) {
    yield put(removeTodoFail(e.message));
  }
}

export function* updateTodo(action) {
  try {
    const client = getClient();
    const result = yield call(client.todo.updateTodo, action.payload);

    if (result.response) {
      yield put(editTodoSuccess(result.message));
      yield put(getAllTodo());
    } else {
      yield put(editTodoFail(result.message));
    }
  } catch (e) {
    yield put(editTodoFail(e.message));
  }
}

export default [
  takeEvery(getAllTodo, getTodo),
  takeEvery(addTodo, newTodo),
  takeEvery(removeTodo, deleteTodo),

  takeEvery(editTodo, updateTodo),
];
