import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

import sagas from './sagas';
import rootReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();

// sagaMiddleware: Makes redux-sagas work
const middlewares = [
  ...getDefaultMiddleware({ thunk: false }),
  sagaMiddleware,
  logger,
];

const store = configureStore({
  reducer: {
    ...rootReducer,
  },
  middleware: middlewares,
  devTools: process.env.NODE_ENV !== 'production',
});

sagaMiddleware.run(sagas);

export default store;
