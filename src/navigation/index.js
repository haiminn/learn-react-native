import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import rootScreens from '../screens';
import DrawerNavigation from './drawerNavigation';

const Stack = createStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Todos" component={DrawerNavigation} />
        <Stack.Screen
          name="TodoDetail"
          component={rootScreens.todoScreen.TodoDetail}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
