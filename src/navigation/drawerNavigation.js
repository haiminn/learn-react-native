import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import rootScreens from '../screens';

const Drawer = createDrawerNavigator();

export default function DrawerNavigation() {
  return (
    <Drawer.Navigator initialRouteName="Todos">
      <Drawer.Screen name="Todos" component={rootScreens.todoScreen.Todos} />
      <Drawer.Screen
        name="FlatListHorizontal"
        component={rootScreens.FlatListHorizontal}
      />
    </Drawer.Navigator>
  );
}
