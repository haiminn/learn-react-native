import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

TodoDetail.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object,
};

function TodoDetail(props) {
  const { navigation, route } = props;
  const { todo } = route.params;

  return (
    <View style={styles.container}>
      <Text>TodoDetail</Text>
      <TouchableOpacity
        style={styles.btnAdd}
        onPress={() => navigation.goBack()}>
        <Text style={styles.txtBtnAdd}>Back to TodoList</Text>
      </TouchableOpacity>
      <Text>{todo.name}</Text>
    </View>
  );
}

export default TodoDetail;
