import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 10,
  },
  btnAdd: {
    padding: 10,
    borderRadius: 2,
    backgroundColor: 'blue',
    marginVertical: 10,
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtBtnAdd: {
    color: 'white',
  },
});

export default styles;
