/* eslint-disable react/prop-types */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Animated,
  Platform,
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';

FlatListHorizontal.propTypes = {};

const { width, height } = Dimensions.get('window');

const BACKDROP_HEIGHT = height * 0.65;

const SPACING = 10;
const ITEM_SIZE = Platform.OS === 'ios' ? width * 0.72 : width * 0.74;
const EMPTY_ITEM_SIZE = (width - ITEM_SIZE) / 2;

function FlatListHorizontal(props) {
  const items = [
    {
      name: 'min1',
      imageUrl: '',
      title: '1',
    },
    {
      name: 'min1',
      imageUrl:
        'https://images.pexels.com/photos/157757/wedding-dresses-fashion-character-bride-157757.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      title: '1',
    },
    {
      name: 'min2',
      imageUrl:
        'https://images.pexels.com/photos/4630002/pexels-photo-4630002.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      title: '1',
    },
    {
      name: 'min3',
      imageUrl:
        'https://images.pexels.com/photos/4322434/pexels-photo-4322434.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      title: '1',
    },
    {
      name: 'min4',
      imageUrl:
        'https://images.pexels.com/photos/4596641/pexels-photo-4596641.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      title: '1',
    },
    {
      name: 'min5',
      imageUrl:
        'https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      title: '1',
    },
    {
      name: 'min1',
      imageUrl: '',
      title: '1',
    },
  ];

  const scrollX = React.useRef(new Animated.Value(0)).current;

  const Backdrop = ({ items, scrollX }) => {
    return (
      <View
        style={{
          height: BACKDROP_HEIGHT,
          width,
          position: 'absolute',
        }}>
        <FlatList
          data={items}
          keyExtractor={(_, index) => index.toString()}
          removeClippedSubviews={false}
          contentContainerStyle={{ width, height: BACKDROP_HEIGHT }}
          renderItem={({ item, index }) => {
            if (!item.imageUrl) {
              return null;
            }
            const translateX = scrollX.interpolate({
              inputRange: [(index - 2) * ITEM_SIZE, (index - 1) * ITEM_SIZE],
              outputRange: [0, width],
              extrapolate: 'clamp',
            });
            return (
              <Animated.View
                key={index}
                removeClippedSubviews={false}
                style={{
                  position: 'absolute',
                  width: translateX,
                  height,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{ uri: item.imageUrl }}
                  style={{
                    width,
                    height: BACKDROP_HEIGHT,
                    position: 'absolute',
                  }}
                />
              </Animated.View>
            );
          }}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Backdrop items={items} scrollX={scrollX} />
      <Animated.FlatList
        showsHorizontalScrollIndicator={false}
        data={items}
        keyExtractor={(item) => item.tittle}
        horizontal
        bounces={false}
        decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
        renderToHardwareTextureAndroid
        contentContainerStyle={{
          alignItems: 'center',
        }}
        snapToInterval={ITEM_SIZE}
        snapToAlignment="start"
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
        scrollEventThrottle={16}
        onRea
        renderItem={({ item, index }) => {
          if (!item.imageUrl) {
            return <View style={{ width: EMPTY_ITEM_SIZE }} />;
          }

          const inputRange = [
            (index - 2) * ITEM_SIZE,
            (index - 1) * ITEM_SIZE,
            index * ITEM_SIZE,
          ];

          const translateY = scrollX.interpolate({
            inputRange,
            outputRange: [100, 50, 100],
            extrapolate: 'clamp',
          });

          return (
            <View key={index} style={{ width: ITEM_SIZE }}>
              <Animated.View
                style={{
                  marginHorizontal: SPACING,
                  padding: SPACING * 2,
                  alignItems: 'center',
                  transform: [{ translateY }],
                  backgroundColor: 'white',
                  borderRadius: 34,
                }}>
                <Image
                  source={{ uri: item.imageUrl }}
                  style={styles.posterImage}
                />
                <Text style={{ fontSize: 24 }} numberOfLines={1}>
                  {item.name}
                </Text>
              </Animated.View>
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  posterImage: {
    width: '100%',
    height: ITEM_SIZE * 1.2,
    resizeMode: 'cover',
    borderRadius: 24,
    margin: 0,
    marginBottom: 10,
  },
});

export default FlatListHorizontal;
