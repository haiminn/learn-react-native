import Todos from './Todos';
import TodoDetail from './TodoDetail';
import FlatListHorizontal from './FlatListHorizontal';

export default {
  todoScreen: {
    Todos,
    TodoDetail,
  },
  FlatListHorizontal,
};
