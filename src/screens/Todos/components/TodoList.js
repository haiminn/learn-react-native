import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import styles from '../styles';

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
  removeTodo: PropTypes.func.isRequired,
  addTodo: PropTypes.func.isRequired,
  changeStatus: PropTypes.func.isRequired,
  goTodoDetail: PropTypes.func.isRequired,
};

function TodoList(props) {
  const { todos, removeTodo, addTodo, changeStatus, goTodoDetail } = props;

  const renderItem = (item, index) => {
    return (
      <View key={index} style={styles.todoItem}>
        <Text style={[styles.txtTodoName, item.done && styles.txtStatusChange]}>
          {item.name}
        </Text>
        <TouchableOpacity
          style={styles.btnChangeStatus}
          onPress={() => goTodoDetail(item)}>
          <Text style={styles.txtDelete}>Go to Detail</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.btnChangeStatus}
          onPress={() => changeStatus(item)}>
          <Text style={styles.txtDelete}>Change status</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnDelete}
          onPress={() => removeTodo(item._id)}>
          <Text style={styles.txtDelete}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const emptyList = () => {
    return (
      <View style={styles.viewEmptyList}>
        <Text>Todo List Empty!</Text>
        <TouchableOpacity style={styles.btnDelete} onPress={addTodo}>
          <Text style={styles.txtDelete}>Click me to add</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <FlatList
      keyExtractor={(_, index) => index.toString()}
      contentContainerStyle={styles.contentContainerStyleFlatList}
      ListEmptyComponent={emptyList}
      data={todos}
      renderItem={({ item, index }) => renderItem(item, index)}
    />
  );
}

export default TodoList;
