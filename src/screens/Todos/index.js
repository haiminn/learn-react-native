import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import useTodos from '../../hooks/useTodos';
import TodoList from './components/TodoList';

Todos.propTypes = {
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object,
};

Todos.defaultProps = {
  title: 'Todos',
};

function Todos(props) {
  const { title, navigation } = props;
  const { todos, onAddTodo, onRemoveTodo, onChangeStatus } = useTodos();

  const goTodoDetail = (todo) => {
    navigation.navigate('TodoDetail', { todo });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.txtTitle}>{title}:</Text>
      <TouchableOpacity style={styles.btnAdd} onPress={onAddTodo}>
        <Text style={styles.txtBtnAdd}>Add Todo</Text>
      </TouchableOpacity>

      <TodoList
        todos={todos}
        removeTodo={onRemoveTodo}
        addTodo={onAddTodo}
        changeStatus={onChangeStatus}
        goTodoDetail={goTodoDetail}
      />
    </View>
  );
}

export default Todos;
