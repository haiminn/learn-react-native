import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  txtTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 10,
  },
  btnAdd: {
    padding: 10,
    borderRadius: 2,
    backgroundColor: 'blue',
    marginVertical: 10,
    marginLeft: 10,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtBtnAdd: {
    color: 'white',
  },
  todoItem: {
    height: 30,
    flexDirection: 'row',
    borderRadius: 2,
    borderWidth: 0.5,
    marginBottom: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    width: Dimensions.get('window').width - 20,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewFlatList: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainerStyleFlatList: {
    flexGrow: 1,
    alignItems: 'center',
  },
  txtTodoName: {
    fontSize: 18,
    color: 'black',
  },
  txtStatusChange: {
    textDecorationLine: 'line-through',
    color: 'orange',
  },
  btnDelete: {
    borderRadius: 2,
    borderWidth: 0.5,
    padding: 2,
    backgroundColor: 'red',
  },
  txtDelete: {
    color: 'white',
  },
  btnChangeStatus: {
    borderRadius: 2,
    borderWidth: 0.5,
    padding: 2,
    backgroundColor: 'green',
  },
  viewEmptyList: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
