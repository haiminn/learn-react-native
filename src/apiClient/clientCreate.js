import Axios from 'axios';
import { AxiosInstance, AxiosResponse } from 'axios';
import { publicHoliday } from './mockData';

export type Config = {
  baseURL: string,
  timeout: number,
  headers: Object,
  debug: boolean,
  test: boolean,
  mock: boolean,
};

export type Auth = {
  id: string,
  secret: string,
};

const POST = 'POST';
const GET = 'GET';
const DELETE = 'DELETE';
const HEAD = 'HEAD';
const OPTIONS = 'OPTIONS';
const PUT = 'PUT';
const PATCH = 'PATCH';

export class ClientError extends Error {
  errorCode: number = -1;

  httpStatus: number = -1;

  responseMessage: string = '';

  constructor(
    message: string,
    errorCode: number = -1,
    httpStatus: number = -1,
  ) {
    super(message);
    this.responseMessage = message;
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
  }
}

const defaultConfig: Config = {
  timeout: 5000,
  headers: {},
  debug: false,
  test: false,
};

function clientCreate(clientConfig: Config, authConfig: Auth) {
  const config = { ...defaultConfig, ...clientConfig };

  if (authConfig) {
    config.headers = {
      ...config.headers,
      tokenId: authConfig.id,
      tokenSecret: authConfig.secret,
    };
  }
  const axios: AxiosInstance = Axios.create(config);
  axios.interceptors.request.use((requestConfig) => {
    if (config.debug) {
      // eslint-disable-next-line no-console
      console.log(requestConfig);
    }
    return requestConfig;
  });
  axios.interceptors.response.use((response) => {
    if (config.debug) {
      // eslint-disable-next-line no-console
      console.log(response);
    }
    return response;
  });

  /**
   *
   * @param method
   * @param url
   * @param data
   * @throws ClientError
   * @returns {Promise<any>}
   */
  async function call(
    method: POST | GET | DELETE | PUT | PATCH | HEAD | OPTIONS,
    url: string,
    data?,
  ) {
    let result: AxiosResponse = null;
    try {
      // eslint-disable-next-line default-case
      switch (method) {
        case POST:
          result = await axios.post(url, data);
          break;
        case GET:
          result = await axios.get(url, data ? { params: data } : {});
          break;
        case DELETE:
          result = await axios.delete(url, { params: data });
          break;
        case PUT:
          result = await axios.post(url, data);
          break;
        case PATCH:
          result = await axios.patch(url, data);
          break;
        case HEAD:
          result = await axios.head(url);
          break;
        case OPTIONS:
          result = await axios.options(url);
          break;
      }
    } catch (e) {
      if (e instanceof ClientError) throw e;
      if (e.isAxiosError) {
        throw new ClientError(
          e.response.data ? e.response.data.message : e.message,
          e.response.data ? e.response.data.error : -1,
          e.response.status,
        );
      }
    }
    if (!result) {
      throw new ClientError(`not fount method. [method=${method}]`);
    }

    return result.data;
  }

  const apiResult = {
    todo: {
      getTodo: (): Promise => call(GET, 'api/todo/'),
      addTodo: (request): Promise => call(POST, 'api/todo/add', request),
      removeTodo: (request): Promise => call(POST, 'api/todo/delete', request),
      updateTodo: (request): Promise => call(POST, 'api/todo/update', request),
    },
    isAuth: !!authConfig,
    equals: (otherConfig: Config, otherAuth: Auth) => {
      return config === otherConfig && authConfig === otherAuth;
    },
  };

  if (config.mock) {
    const newApiResult = { ...apiResult };
    newApiResult.calendar = {
      getPublicHoliday: (): Promise<Object> =>
        new Promise((success) => {
          success(publicHoliday);
        }),
    };

    return newApiResult;
  }

  return apiResult;
}

export default clientCreate;
