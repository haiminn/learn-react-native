import clientCreate from './clientCreate';
import config from '../../config/config';
// import type { Auth, Config } from './clientCreate';

function getClient(auth) {
  const clientConfig = {
    baseURL: config.baseURL,
    mock: config.mock,
  };
  return clientCreate(clientConfig);
}

export default getClient;
